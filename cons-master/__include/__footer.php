<footer class="text-center pos-re">
    <div class="container">
        <div class="footer__box">
            <!-- Logo -->
            <a class="logo" style="text-decoration: inherit;font-size: 35px;color: #fff;">
                <!--<img src="img/logo-light.png" alt="logo">-->
                مبل اتحاد
            </a>

            <div class="social">
                <a href="#0"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="#0"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="#0"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>

            <p> تمامی حقوق این سایت متعلق به وبسایت مبل اتحاد می باشد</p>
        </div>
    </div>

    <div class="curve curve-top curve-center"></div>
</footer>

<?php if (!isset($no_js)){ ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/modernizr.custom.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/slick.min.js"></script>
    <script src="../js/scrollreveal.min.js"></script>
    <script src="../js/jquery.cubeportfolio.min.js"></script>
    <script src="../js/jquery.matchHeight-min.js"></script>
    <script src="../js/masonry.pkgd.min.js"></script>
    <script src="../js/jquery.flexslider-min.js"></script>
    <script src="../js/classie.js"></script>
    <script src="../js/helper.js"></script>
    <script src="../js/grid3d.js"></script>
    <script src="../js/script.js"></script>
<?php } ?>


</body>
</html>