-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 28, 2019 at 06:34 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `etehad`
--

-- --------------------------------------------------------

--
-- Table structure for table `board_words`
--

CREATE TABLE `board_words` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `confirmed` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `board_words`
--

INSERT INTO `board_words` (`id`, `user`, `text`, `confirmed`) VALUES
(1, 1, 'نظر هیئت مدیره اینه', 1),
(2, 1, 'یه چیزی', 1),
(3, 1, 'یه چیزی', 1),
(4, 1, 'یه نظری', 1),
(5, 1, 'نظر ما اینه', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `confirm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `phone`, `subject`, `text`, `confirm`) VALUES
(1, 'پوریا خزایی', 'kpouriya6@gmail.com', '', 'سلام', 'یه چیزی', 1),
(2, 'پوریا خزایی', '', '', '', 'سلام', 1),
(3, 'پوریا خزایی', '', '', '', 'سلام', 1),
(4, 'پوریا خزایی', '', '', '', 'خدا حافظ', 1),
(5, 'پوریا خزایی', '', '', '', 'خداحافظ', 1),
(6, 'پوریا خزایی', '', '', '', 'خداحافظ', 1),
(7, 'پوریا خزایی', '', '', '', 'خداحافظ', 1),
(8, 'پوریا خزایی', '', '', '', 'هشتگ', 1),
(9, 'پوریا خزایی', '', '', '', 'مبل', 1),
(10, 'پوریا خزایی', '', '', '', 'هی', 1),
(11, 'پوریا خزایی', '', '', '', 'سلام', 1),
(12, 'پوریا خزایی', '', '', '', 'سلام', 1),
(13, 'پوریا خزایی', '', '', '', 'خدا حافظ', 1),
(14, 'پوریا خزایی', '', '', '', 'خداحافظ', 1),
(15, 'پوریا خزایی', '', '', '', 'خداحافظ', 1),
(16, 'پوریا خزایی', '', '', '', 'خداحافظ', 1),
(17, 'پوریا خزایی', '', '', '', 'هشتگ', 1),
(18, 'پوریا خزایی', '', '', '', 'مبل', 1),
(19, 'پوریا خزایی', '', '', '', 'هی', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sofas`
--

CREATE TABLE `sofas` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `images_count` int(11) NOT NULL,
  `available` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sofas`
--

INSERT INTO `sofas` (`id`, `name`, `category`, `description`, `price`, `images_count`, `available`) VALUES
(4, 'مبل سلطنتی', 1, 'مبل سلطنتی کرم رنگ', 1, 1, 1),
(5, 'مبل سلطنتی', 1, 'مبل سلطنتی سفید', 1, 1, 1),
(6, 'مبل مجلسی', 1, 'مبل مجلسی کرم رنگ', 1, 1, 1),
(7, 'مبل سلطنتی صورتی', 1, 'مبل سلطنتی صورتی', 1, 1, 1),
(8, 'مبل سلطنتی سفید خاکستری', 1, 'مبل سلطنتی سفید خاکستری', 1, 1, 1),
(9, 'مبل کلاسیک نقره ای', 1, 'مبل کلاسیک نقره ای', 1, 1, 1),
(10, 'مبل سلطنتی زرد', 1, 'مبل سلطنتی زرد', 1, 1, 1),
(11, 'مبل سلطنتی قرمز', 1, 'مبل سلطنتی قزمر', 1, 1, 1),
(12, 'مبل سلطنتی و شیک سفید', 1, 'مبل سلطنتی و شیک سفید', 1, 1, 1),
(13, 'مبل سلطنتی شیک و صورتی', 1, 'مبل سلطنتی شیک و صورتی', 1, 1, 1),
(14, 'مبل سلطنتی فیروزه ای', 1, 'مبل سلطنتی فیروزه ای', 1, 1, 1),
(15, 'مبل کلاسیک و شیک سفید', 1, 'مبل کلاسیک و شیک سفید', 1, 1, 1),
(16, 'مبل کلاسیک آبی', 1, 'مبل کلاسیک آبی', 1, 1, 1),
(17, 'مبل های تک نفره قهوه ای', 1, 'مبل های تک نفره قهوه ای مخصوص میز ناهار خوری', 1, 1, 1),
(18, 'مبل های سلطنتی با روکش سنتی', 1, 'مبل های سلطنتی با طرح سنتی', 1, 1, 1),
(19, 'مبل سلطنتی سفید چوبی', 1, 'مبل های سلطنتی سفید با دسته های چوبی', 1, 2, 1),
(20, 'مبل تک نفره سفید', 1, 'مبل های تک نفره و سبک سفید مخصوص میزهای غذاخوری', 1, 1, 1),
(21, 'مبل های سلطنتی و شیک آبی', 1, 'مبل های سلطنتی و شیک آبی', 1, 1, 1),
(22, 'مبل سلطنتی و مجلسی سفید', 1, 'مبل سلطنتی و مجلسی سفید', 1, 1, 1),
(23, 'مبل مجلسی سفید', 1, 'مبل مجلسی سفید', 1, 1, 1),
(24, 'ست مبل های قهوه ای', 1, 'ست مبل های قهوه ای', 1, 3, 1),
(25, 'صندلی های غذا خوری', 1, 'صندلی های غذا خوری', 1, 1, 1),
(26, 'ست مبل های سبز', 1, 'ست مبل های سبز', 1, 2, 1),
(27, 'ست مبل شیک سفید', 1, 'ست مبل شیک سفید', 1, 10, 1),
(28, 'مبل سلطنتی سبز', 1, 'مبل سلطنتی سبز', 1, 1, 1),
(29, 'ست مبل صورتی', 1, 'ست مبل های سلطتنتی و صندلی های صورتی', 1, 3, 1),
(30, 'ست مبل های سلطنتی', 1, 'ست مبل ها و صندلی های شیک و سلطنتی', 1, 6, 1),
(31, 'ست مبل های سلطنتی و شیک', 1, 'ست مبل های سلطنتی و شیک', 1, 3, 1),
(32, 'ست میز و مبل های سلطنتی', 1, 'ست میز و مبل های سلطنتی', 1, 1, 1),
(33, 'مبل های طرح سنتی', 1, 'مبل های طرح سنتی', 1, 1, 1),
(34, 'صندلی قرمز', 1, 'صندلی قرمز', 1, 1, 1),
(35, 'ست مبل های سبز', 1, 'ست مبل های سبز', 1, 1, 1),
(36, 'مبل سلطنتی فیروزه ای', 1, 'مبل سلطنتی فیروزه ای', 1, 1, 1),
(37, 'مبل کلاسیک سبز', 1, 'مبل کلاسیک سبز', 1, 1, 1),
(38, 'مبل سلطنتی سبز', 1, 'مبل سلطنتی سبز', 1, 1, 1),
(39, 'ست مبل سلطنتی سبز', 1, 'ست مبل سلطنتی سبز', 1, 1, 1),
(40, 'مبل های کلاسیک سفید', 1, 'مبل های کلاسیک سفید', 1, 2, 1),
(41, 'مبل کلاسیک', 1, 'مبل کلاسیک', 1, 3, 1),
(42, 'مبل فانتزی', 1, 'مبل فانتزی', 1, 2, 1),
(43, 'مبل کلاسیک قرمز', 1, 'مبل کلاسیک قرمز', 1, 1, 1),
(44, 'مبل های سلطنتی', 1, 'مبل های سلطنتی', 1, 2, 1),
(45, 'مبل کلاسیک قهوه ای', 1, 'مبل کلاسیک قهوه ای', 1, 1, 1),
(46, 'مبل سلطنتی سفید', 1, 'مبل سلطنتی سفید', 1, 1, 1),
(47, 'مبل سلطنتی آبی', 1, 'مبل سلطنتی آبی', 1, 1, 1),
(48, 'مبل شیک و کلاسیک قهوه ای', 1, 'مبل شیک و کلاسیک قهوه ای', 1, 1, 1),
(49, 'مبل فانتزی آبی', 1, 'مبل فانتزی آبی', 1, 5, 1),
(50, 'مبل زیبای سفید', 1, 'مبل زیبای سفید', 1, 5, 1),
(51, 'ست مبل های فانتزی', 1, 'ست مبل های فانتزی', 1, 2, 1),
(52, 'مبل فانتزی سبز', 1, 'مبل فانتزی سبز', 1, 5, 1),
(53, 'ست مبل های شیک و سلطنتی', 1, 'ست مبل های شیک و سلطنتی', 1, 10, 1),
(54, 'مبل های سفید سلطنتی', 1, 'مبل های سفید سلطنتی', 1, 1, 1),
(55, 'مبل های شیک و کلاسیک سفید', 1, 'مبل های شیک و کلاسیک سفید', 1, 1, 1),
(56, 'ست صندلی های سفید غذاخوری', 1, 'ست صندلی های سفید غذاخوری', 1, 1, 1),
(57, 'مبل سلطنتی بنفش', 1, 'مبل سلطنتی بنفش', 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sofa_categories`
--

CREATE TABLE `sofa_categories` (
  `id` int(11) NOT NULL,
  `caption` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sofa_categories`
--

INSERT INTO `sofa_categories` (`id`, `caption`, `description`) VALUES
(1, 'مبل راحتی', 'مبل هایی بسیار راحت'),
(2, 'مبل های سلطنتی', 'مبل های کلاسیک و شیک'),
(3, 'کاناپه', 'مبل های راحت جادار');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `accessibility` int(11) NOT NULL DEFAULT '0',
  `facebook` text COLLATE utf8_unicode_ci NOT NULL,
  `twitter` text COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` text COLLATE utf8_unicode_ci NOT NULL,
  `telegram` text COLLATE utf8_unicode_ci NOT NULL,
  `instagram` text COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `accessibility`, `facebook`, `twitter`, `linkedin`, `telegram`, `instagram`, `role`) VALUES
(1, 'مهدی بیاتلو', 0, '', '', '', '', '', 'مدیر');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `board_words`
--
ALTER TABLE `board_words`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sofas`
--
ALTER TABLE `sofas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sofa_categories`
--
ALTER TABLE `sofa_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `board_words`
--
ALTER TABLE `board_words`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `sofas`
--
ALTER TABLE `sofas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `sofa_categories`
--
ALTER TABLE `sofa_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
