<?php
if (!isset($c)){
    require '__config.php';
}
session_start();
if (!isset($con)){
    $con = new mysqli(Host,User,Password,DB);
    $con->set_charset('utf8');
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <style>
        *{
            font-family: Vazir;
        }
    </style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1"/>
    <meta name="author" content="Theme Industry">
    <!-- description -->
    <meta name="description" content="boltex">
    <!-- keywords -->
    <meta name="keywords" content="">
    <title>
        مبل اتحاد<?php if (isset($Page_Title)){echo ' | '.$Page_Title;} ?>
    </title>
    <link href="fonts/sofa/styles.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CRoboto%7CJosefin+Sans:100,300,400,500" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../fonts/Vazir/Vazir.css">
    <!-- <link rel="stylesheet" href="css/bootstrap3.min.css"> -->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/magnific-popup.css">
    <link rel="stylesheet" href="../css/slick.css">
    <link rel="stylesheet" href="../css/cubeportfolio.min.css">
    <link rel="stylesheet" type="text/css" href="../css/component.css" />
</head>
<body>
<nav class="navbar navbar-expand-lg fixed-top activate-menu navbar-light bg-light">
    <a class="navbar-brand" href="/">مبل اتحاد</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse"    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li >
                <a class="nav-link" href="/">خانه</a>
            </li>
            <li>
                <a class="nav-link" href="sofas">مبل ها</a>
            </li>

            <li>
                <a class="nav-link" href="#contact">تماس با ما</a>
            </li>
            <li>
                <a class="nav-link" href="about-us">درباره ما</a>
            </li>
        </ul>
    </div>
</nav>


