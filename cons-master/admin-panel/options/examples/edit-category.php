<?php
$ssion = 1;
session_start();
if (isset($_SESSION['id'])){
    if ($_SESSION['accessibility'] >= 2){
        $active = 'category';
        require 'header.php';
        if (isset($_GET['q'])){
            $stmt = $con->prepare('SELECT * FROM `sofa_categories` WHERE id = ?');
            $stmt->bind_param('i',$_GET['q']);
            $stmt->execute();
            $categories = $stmt->get_result();
            $category = $categories->fetch_assoc();
        }
        ?>
        <style>
            .form-group{
                padding: 20px;
            }
        </style>
        <div class="content">
            <div class="container-fluid">
                <form method="post" enctype="multipart/form-data" action="edit-operation.php?type=category<?php echo isset($category) ? '&q='.$category['id'] : ''; ?>">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان دسته بندی</label>
                        <input required type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="عنوان" value="<?PHP echo $category['caption'] ?? ''; ?>">
                        <!--                     <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">توضیح دسته بندی</label>
                        <textarea name="desc" class="form-control" id="exampleFormControlTextarea1" rows="3"><?php echo $category['description'] ?? ''; ?></textarea>
                    </div>
                    <!--<div class="fileinput fileinput-new text-center" data-provides="fileinput">
                    <?php
                    if (isset($_GET['q'])){
                        ?><div class="fileinput-new thumbnail img-raised">
                            <img src="../../../img/Sofas/<?php echo $sofa['sofa_id'] ?? ''; ?>/1.jpg" alt="...">
                        </div>
                        <?php
                    }
                    ?>
                     <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                    <div>
        <span class="btn btn-raised btn-round btn-default btn-file">
            <span class="fileinput-new">Select image</span>
            <span class="fileinput-exists">Change</span>
            <input type="file" name="img" />
        </span>
                        <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                    </div>
                </div>-->

                    <button type="submit" class="btn btn-primary" style="width: 20%;margin: auto;display: block">ثبت</button>
                </form>
            </div>
        </div>
        <?php
        require 'footer.php';
    }
    else{
        header('location: /');
    }
}
else{
    header('location: /');
}
?>