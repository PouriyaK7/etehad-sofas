<?php
$ssion = 1;
session_start();
if (isset($_SESSION['id'])){
    if ($_SESSION['accessibility'] >= 2){

        if(isset($_GET['type'],$_GET['q'])){
            require '../../../__include/__config.php';
            $con = new mysqli(Host,User,Password,DB);
            $con->set_charset('utf8');
            switch ($_GET['type']){
                case 'sofa':
                    $stmt = $con->prepare('DELETE FROM `sofas` WHERE `id` = ?');
                    $stmt->bind_param('i',$_GET['q']);
                    $stmt->execute();
                    header('location: sofas.php');
                    break;
                case 'bed':
                    break;
                case 'cabinet':
                    break;
                case 'category':
                    $stmt = $con->prepare('DELETE FROM `sofa_categories` WHERE `id` = ?');
                    $stmt->bind_param('i',$_GET['q']);
                    $stmt->execute();
                    header('location: categories.php');
                    break;
                case 'user':
                    if ($_SESSION['Accessibility'] >= 3){
                        $stmt = $con->prepare('DELETE FROM `users` WHERE `id` = ?');
                        $stmt->bind_param('i',$_GET['q']);
                        $stmt->execute();
                        header('location: users.php');
                    }
                    else{
                        header('location: /');
                    }
                    break;
            }
        }
        else{
            header('location: /');
        }
    }
    else{
        header('location: /');
    }
}
else{
    header('location: /');
}