<?php
$active = 'sofa';
$ssion = 1;
session_start();
if (isset($_SESSION['id'])) {
    if ($_SESSION['accessibility'] >= 2) {
        require 'header.php';
        $stmt = $con->prepare('SELECT *, sofas.id AS `sofa_id`, sofas.description AS `sofa_description` FROM sofas INNER JOIN sofa_categories ON sofa_categories.id = sofas.category GROUP BY sofas.id ORDER BY sofas.id DESC ');
        $stmt->execute();
        $sofas = $stmt->get_result();
        ?>
        <div class="content">
            <div class="container-fluid">
                <h1 style="direction: rtl;">
                    مبل ها
                    <a class="btn btn-primary btn-round" href="edit-sofa.php">
                        اضافه کردن
                    </a>
                </h1>
                <div class="row">
        <?php
        while ($sofa = $sofas->fetch_assoc()) {
            ?>
            <div class="card" style="margin-right: 20px;width: 20rem;">
                <img class="card-img-top" style="width: 320px;height: 320px;" src="../../../img/Sofas/<?php echo $sofa['sofa_id']; ?>/1.jpg" alt="<?php echo $sofa['name'] ?>">
                <div class="card-body">
                    <p class="card-text">
                        نام مبل: <?php echo $sofa['name']; ?>
                        <br>

                        دسته بندی مبل: <?php echo $sofa['caption']; ?>
                        <br>
                        <a href="edit-sofa.php?q=<?php echo $sofa['sofa_id']; ?>">
                            <button type="button" class="my-hover btn btn-default btn-link" rel="tooltip" data-placement="top" title="" style="background-color: #9c27b0;width: 30px;height: 30px;min-width: 30px;color:#fff;line-height: 4px;padding: 0;border-radius: 50%;box-shadow: 1px 1px 3px rgba(0,0,0,.3)" data-original-title="edit">
                                <i class="material-icons">edit</i>
                            </button>
                        </a>
                        <a href="delete.php?type=sofa&q=<?php echo $sofa['sofa_id']; ?>">
                            <button type="button" class="my-hover btn btn-default btn-link" rel="tooltip" data-placement="top" title="" style="background-color: #9c27b0;width: 30px;height: 30px;min-width: 30px;color:#fff;line-height: 4px;padding: 0;border-radius: 50%;box-shadow: 1px 1px 3px rgba(0,0,0,.3)" data-original-title="delete">
                                <i class="material-icons">delete</i>
                            </button>
                        </a>
                    </p>
                </div>
            </div>
            <?php
        }
        ?>
        </div>
                </div>
            </div>
        <?php
        require 'footer.php';
    }
    else{
        header('location: /');
    }
}
else{
    header('location: /');
}
