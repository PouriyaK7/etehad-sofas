<?php
$active = 'dashboard';
$ssion = 1;
session_start();
if (isset($_SESSION['id'])){
    if ($_SESSION['accessibility'] >= 3){
        require 'header.php';
        if (isset($_POST['name'],$_POST['password'],$_POST['role'])){
            $stmt = $con->prepare('UPDATE `users` SET users.name = ?, users.role = ?, users.password = ? WHERE `users`.`id` = ?');
            $pass = password_hash($_POST['password'],PASSWORD_DEFAULT);
            $stmt->bind_param('sssi',$_POST['name'],$_POST['role'],$pass,$_SESSION['id']);
            $stmt->execute();
            $users = $stmt->get_result();
            $_SESSION['name'] = $_POST['name'];
            $_SESSION['role'] = $_POST['role'];
        }
        ?>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8 m-auto">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title" style="text-align: right">تغییر حساب کاربری</h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body">
                                <form method="post">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">نام</label>
                                                <input type="text" name="name" class="form-control" value="<?php echo $_SESSION['name']; ?>">
                                            </div>
                                        </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">سمت</label>
                                                    <input type="text" name="role" class="form-control" value="<?php echo $_SESSION['role']; ?>">
                                                </div>
                                            </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">گذرواژه</label>
                                                <input type="password" name="password" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>About Me</label>
                                                <div class="form-group">
                                                    <label class="bmd-label-floating"> Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo.</label>
                                                    <textarea class="form-control" rows="5"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    -->
                                    <button type="submit" class="btn btn-primary pull-right">تغییر حساب کابری</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        require 'footer.php';
    }
    else{
        header('location: /');
    }
}
else{
    header('location: /');
}