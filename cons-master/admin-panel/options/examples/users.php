<?php
$active = 'user-manager';
$ssion = 1;
session_start();
if (isset($_SESSION['id'])){
    if ($_SESSION['accessibility'] >= 3){
        require 'header.php';
        $stmt = $con->prepare('SELECT * FROM `users` WHERE `id` != ?');
        $stmt->bind_param('i',$_SESSION['id']);
        $stmt->execute();
        $users = $stmt->get_result();
        ?>
        <div class="content">
        <div class="container-fluid">
        <h1 style="direction: rtl;">
            ادمین ها
            <a class="btn btn-primary btn-round" href="edit-user.php">
                اضافه کردن
            </a>
        </h1>
        <div class="row">
        <?php
        while ($user = $users->fetch_assoc()) {
            ?>
            <div class="card" style="margin-right: 20px;width: 20rem;">
                <div class="card-body">
                    <p class="card-text">
                         <?php echo $user['name']; ?>
                         <br>
                         سمت:
                         <?php echo $user['role']; ?>
                        <br>
                        <a href="edit-user.php?q=<?php echo $user['id']; ?>">
                            <button type="button" class="my-hover btn btn-default btn-link" rel="tooltip" data-placement="top" title="" style="background-color: #9c27b0;width: 30px;height: 30px;min-width: 30px;color:#fff;line-height: 4px;padding: 0;border-radius: 50%;box-shadow: 1px 1px 3px rgba(0,0,0,.3)" data-original-title="edit">
                                <i class="material-icons">edit</i>
                            </button>
                        </a>
                        <a href="delete.php?type=user&q=<?php echo $user['id']; ?>">
                            <button type="button" class="my-hover btn btn-default btn-link" rel="tooltip" data-placement="top" title="" style="background-color: #9c27b0;width: 30px;height: 30px;min-width: 30px;color:#fff;line-height: 4px;padding: 0;border-radius: 50%;box-shadow: 1px 1px 3px rgba(0,0,0,.3)" data-original-title="delete">
                                <i class="material-icons">delete</i>
                            </button>
                        </a>
                    </p>
                </div>
            </div>
            <?php
        }
        echo '</div></div></div>';
        require 'footer.php';
    }
    else{
        header('location: /');
    }
}
else{
    header('location: /');
}