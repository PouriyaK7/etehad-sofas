<?php
$ssion = 1;
session_start();
if (isset($_SESSION['id'])){
    if ($_SESSION['accessibility'] >= 2){
        require '../../../__include/__config.php';
        $con = new mysqli(Host,User,Password,DB);
        $con->set_charset('utf8');
        if (isset($_GET['type'],$_GET['q'])){
            switch ($_GET['type']){
                case 'sofa':
                    $stmt = $con->prepare('UPDATE `sofas` SET `name` = ?, `category` = ?, `description` = ?, `price` = ? WHERE id = ?');
                    $stmt->bind_param('sisii',$_POST['name'],$_POST['category'],$_POST['desc'],$_POST['price'],$_GET['q']);
                    $stmt->execute();
                    header('location: sofas.php');
                    break;
                case 'category':
                    $stmt = $con->prepare('UPDATE `sofa_categories` SET `caption` = ?, `description` = ? WHERE id = ?');
                    $stmt->bind_param('ssi',$_POST['name'],$_POST['desc'],$_GET['q']);
                    $stmt->execute();
                    header('location: categories.php');
                    break;
                case 'user':
                    if ($_SESSION['accessibility'] >= 3){
                        $pass = password_hash($_POST['password'],PASSWORD_DEFAULT);
                        $stmt = $con->prepare('UPDATE `users` SET `name` = ?, `role` = ?, `password` = ? WHERE id = ?');
                        $stmt->bind_param('sssi',$_POST['name'],$_POST['desc'],$pass,$_GET['q']);
                        $stmt->execute();
                        header('location: users.php');
                    }
                    else{
                        header('location: /');
                    }
                    break;
            }
        }
        elseif (isset($_GET['type'])){
            switch ($_GET['type']){
                case 'sofa':
                    $stmt = $con->prepare('INSERT INTO `sofas`(`name`,`category`,`description`,`price`) VALUES (?,?,?,?)');
                    $stmt->bind_param('sisi',$_POST['name'],$_POST['category'],$_POST['desc'],$_POST['price']);
                    $stmt->execute();
                    header('location: sofas.php');
                    break;
                case 'category':
                    $stmt = $con->prepare('INSERT INTO `sofa_categories`(`caption`,`description`) VALUES (?,?)');
                    $stmt->bind_param('ss',$_POST['name'],$_POST['desc']);
                    $stmt->execute();
                    header('location: categories.php');
                    break;
                case 'user':
                    if ($_SESSION['accessibility'] >= 3){
                        $pass = password_hash($_POST['password'],PASSWORD_DEFAULT);
                        $a = 2;
                        $stmt = $con->prepare('INSERT INTO `users`(`name`,`role`,`accessibility`,`password`) VALUES (?,?,?,?)');
                        $stmt->bind_param('ssis',$_POST['name'],$_POST['desc'],$a,$pass);
                        $stmt->execute();
                        header('location: users.php');
                    }
                    else{
                        header('location: /');
                    }
                    break;
            }
        }
    }
    else{
        header('location: /');
    }
}
else{
    header('location: /');
}