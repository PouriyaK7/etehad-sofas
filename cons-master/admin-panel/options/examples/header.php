<?php
if (!isset($ssion)){
    session_start();
}
if (!isset($config)){
    require '../../../__include/__config.php';
}
$con = new mysqli(Host,User,Password,DB);
$con->set_charset('utf8');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        پنل ادمین
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet" />
</head>

<body class="">
<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
        <!--
          Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

          Tip 2: you can also add an image using data-image tag
      -->
        <div class="logo">
            <a href="/admin-panel/options/examples" class="simple-text logo-normal">
                مبل اتحاد
            </a>
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="nav-item <?php echo $active == 'dashboard' ? 'active' : ''; ?>">
                    <a class="nav-link" href="index.php">
                        <i class="material-icons">dashboard</i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item <?php echo $active == 'sofa' ? 'active' : ''; ?>">
                    <a href="sofas.php" class="nav-link">
                        <i class="material-icons">event_seat</i>
                        <p>
                            مبل ها
                        </p>
                    </a>
                </li>
                <li class="nav-item <?php echo $active == 'category' ? 'active' : ''; ?>">
                    <a href="categories.php" class="nav-link">
                        <i class="material-icons">category</i>
                        <p>
                            دسته بندی ها
                        </p>
                    </a>
                </li>
                <?php if ($_SESSION['accessibility'] >= 3){ ?>
                    <li class="nav-item <?php echo $active == 'user-manager' ? 'active' : ''; ?>">
                        <a href="users.php" class="nav-link">
                            <i class="material-icons">supervised_user_circle</i>
                            <p>
                                مدیریت ادمین ها
                            </p>
                        </a>
                    </li>
                <?php } ?>
                <li class="nav-item">
                    <a href="logout.php" class="nav-link">
                        <i class="material-icons">close</i>
                        <p>
                            خروج از حساب
                        </p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <a class="navbar-brand" href="/">مبل اتحاد</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">person</i>
                                <p class="d-lg-none d-md-block">
                                    Account
                                </p>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                                <a class="dropdown-item" href="#"><?php echo $_SESSION['name']; ?></a>
                                <div class="dropdown-divider"></div>
                                <?php if ($_SESSION['accessibility'] >= 3){ ?>
                                    <a class="dropdown-item" href="profile.php">Profile</a>
                                <?php } ?>
                                <a class="dropdown-item" href="logout.php">Log out</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>