<?php
$active = 'category';
$ssion = 1;
session_start();
if (isset($_SESSION['id'])){
    if ($_SESSION['accessibility'] >= 2){
        require 'header.php';
        $stmt = $con->prepare('SELECT * FROM `sofa_categories` ORDER BY `id` DESC ');
        $stmt->execute();
        $categories = $stmt->get_result();
        ?>
        <div class="content">
        <div class="container-fluid">
        <h1 style="direction: rtl;">
            دسته بندی ها
            <a class="btn btn-primary btn-round" href="edit-category.php">
                اضافه کردن
            </a>
        </h1>
        <div class="row">
        <?php
        while ($category = $categories->fetch_assoc()) {
            ?>
            <div class="card" style="margin-right: 20px;width: 20rem;">
                <img class="card-img-top" style="width: 320px;height: 320px;" src="../../../img/Categories/<?php echo $category['id']; ?>.jpg" alt="<?php echo $category['caption'] ?>">
                <div class="card-body">
                    <p class="card-text">
                         <?php echo $category['caption']; ?>
                        <br>
                        <a href="edit-category.php?q=<?php echo $category['id']; ?>">
                            <button type="button" class="my-hover btn btn-default btn-link" rel="tooltip" data-placement="top" title="" style="background-color: #9c27b0;width: 30px;height: 30px;min-width: 30px;color:#fff;line-height: 4px;padding: 0;border-radius: 50%;box-shadow: 1px 1px 3px rgba(0,0,0,.3)" data-original-title="edit">
                                <i class="material-icons">edit</i>
                            </button>
                        </a>
                        <a href="delete.php?type=category&q=<?php echo $category['id']; ?>">
                            <button type="button" class="my-hover btn btn-default btn-link" rel="tooltip" data-placement="top" title="" style="background-color: #9c27b0;width: 30px;height: 30px;min-width: 30px;color:#fff;line-height: 4px;padding: 0;border-radius: 50%;box-shadow: 1px 1px 3px rgba(0,0,0,.3)" data-original-title="delete">
                                <i class="material-icons">delete</i>
                            </button>
                        </a>
                    </p>
                </div>
            </div>
            <?php
        }
        echo '</div></div></div>';
        require 'footer.php';
    }
    else{
        header('location: /');
    }
}
else{
    header('location: /');
}