
<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<?PHP
$active = 'dashboard';
$ssion = 1;
session_start();
if (isset($_SESSION['id'])) {
    if ($_SESSION['accessibility'] >= 2) {
        require 'header.php';
        ?>
        <!-- End Navbar -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-warning card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">event_seat</i>
                                </div>
                                <p class="card-category">مبل</p>
                                <h3 class="card-title">
                                    مبل ها
                                </h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats" style="text-align: right">
                                    <a href="sofas.php" class="btn btn-primary btn-round">دیدن همه</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">category</i>
                                </div>
                                <p class="card-category">دسته بندی</p>
                                <h3 class="card-title">دسته بندی ها</h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <a href="categories.php" class="btn btn-primary btn-round">
                                        دیدن همه
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-danger card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">perm_contact_calender</i>
                                </div>
                                <p class="card-category">تماس با ما</p>
                                <h3 class="card-title">پیشنهادات</h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <a href="" class="btn btn-primary btn-round">
                                        دیدن همه
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    -->
                    <!--
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-info card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">record_voice_over</i>
                                </div>
                                <p class="card-category">سخنان همکاران</p>
                                <h3 class="card-title">نظرات تیم</h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <a href="" class="btn btn-primary btn-round">
                                        دیدن همه
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    -->
                    <?php if ($_SESSION['accessibility'] >= 3){ ?>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header card-header-icon">
                                    <div class="card-icon">
                                        <i class="material-icons">supervised_user_circle</i>
                                    </div>
                                    <p class="card-category">مدیریت</p>
                                    <h3 class="card-title">
                                        ادمین ها
                                    </h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <a href="users.php" class="btn btn-primary btn-round">
                                            دیدن همه
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php
        require 'footer.php';
    }
    else{
        header('location: /');
    }
}
else{
    header('location: /');
}