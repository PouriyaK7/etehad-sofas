<?php
$Page_Title = 'دسته بندی ها';
require '__include/__header.php';
?>
    <div id="showcase">
    <div class="container showcase">
        <div class="full-width text-center showcase-caption mt-30">
            <h4 style="direction: rtl">مبل اتحاد</h4>
            <h1 style="direction: rtl">تمامی دسته بندی های مبل اتحاد</h1>
            <p style="direction: rtl">کیفیت بالا با قیمت مناسب</p>
        </div>
    </div>
    </div>
    <div id="blog" class="blog">
        <div class="container">
            <div class="teams-heading text-center col-md-8 offset-md-2 col-sm-12 text-center">
                <!-- <span>Heros Behind The Company</span> -->
                <h1 class="teams-heading" style="position: relative;color: #fff;direction: rtl">
                    دسته بندی ها
                </h1>
            </div>
            <div class="row">
                <?php
                $stmt = $con->prepare('SELECT * FROM `sofa_categories` ORDER BY `id` DESC LIMIT ?');
                $limit = 3;
                $stmt->bind_param('i',$limit);
                $stmt->execute();
                $categories = $stmt->get_result();
                while ($category = $categories->fetch_assoc()){
                    ?>
                    <div class="col-sm">
                        <div class="blog-item-box">
                            <figure class="blog-item">
                                <div class="image">
                                    <img style="width: 310px;height: 189.88px" src="img/Categories/<?php echo $category['id']; ?>.jpg" alt="sofa-<?php echo $category['caption']; ?>"/>

                                    <i class="fa fa-list-alt" aria-hidden="true"></i>

                                    <div class="date"><span class="day" style="padding-bottom: 50%">مبل</span><span class="month"></span></div>
                                </div>
                                <figcaption>
                                    <h3 style="direction: rtl">
                                        <?php echo $category['caption']; ?>
                                    </h3>
                                    <p style="direction: rtl">
                                        <?php echo $category['description']; ?>
                                    </p>
                                    <a href="single-category?q=<?php echo $category['id']; ?>" class="read-more" style="font-weight: bold;font-size: 15px;">
                                        مشاهده بیشتر
                                    </a>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                <?php } ?>
                <!--
                <div class="col-sm">
                  <div class="blog-item-box">
                    <figure class="blog-item">
                        <div class="image">
                          <img src="./img/sample84.jpg" alt="sample87"/>
                          <i class="fa fa-clock-o" aria-hidden="true"></i>
                          <div class="date"><span class="day">22</span>
                            <span class="month">خرداد</span></div>
                        </div>
                        <figcaption>
                          <h3>
                              نمونه کار سلطنتی
                          </h3>
                          <p>
                              مبل های سلطنتی با لعاب طلا در تاج با کیفیت بالا و قیمت پایین فقط در مبل اتحاد
                          </p>
                          <a href="#" class="read-more">Read More</a>
                        </figcaption>
                      </figure>
                    </div>
                </div>
                <div class="col-sm">
                  <div class="blog-item-box">
                    <figure class="blog-item">
                        <div class="image">
                          <img src="./img/sample87.jpg" alt="sample84"/>
                          <i class="fa fa-clock-o" aria-hidden="true"></i>
                          <div class="date"><span class="day">26</span><span class="month">شهریور</span></div>
                        </div>
                        <figcaption>
                          <h3>یک نمونه کار دیگه از مبل های سلطنتی</h3>
                          <p>
                              می دونید ما به چی نیاز داریم، سرگرمی؟ ما به نگرش نیاز داریم. شما نمی تونید باحال باشید اگه نگرش نداشته باشید؛ پس انتقادات و پیشنهادات خود را از انتهای همین صفحه بفرستید تا نگرش ما را به سبک خود دراورید
                          </p>
                          <a href="#" class="read-more">Read More</a>
                        </figcaption>
                      </figure>
                  </div>
                </div>
                  -->
            </div>
        </div>
    </div>
<?php
require '__include/__footer.php';