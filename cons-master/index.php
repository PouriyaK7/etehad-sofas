<?php
require '__include/__header.php';
?>
    <style>
        .hi-man{
            width: 100%;
            text-align: center;
        }
        .hi-man .col-lg-3{
            display: inline-block;
        }
        .hi-man .folded-corner{
            height: 185px;
        }
    </style>
    <!--================ Showcase section ===================-->

    <div id="showcase">
        <div class="container showcase">
            <div class="full-width text-center showcase-caption mt-30">
                <h4>مبل اتحاد</h4>
                <h1>تولیدی انواع مبل</h1>
                <p>کیفیت بالا با قیمت مناسب</p>
                <div class="showcase-button">
                    <a href="#features" class="button-style showcase-btn" style="direction: rtl">
                        اطلاعات بیشتر
                    </a>
                    <a href="#services" class="button-style showcase-btn" style="direction: rtl">
                        شروع کنیم!
                    </a>
                </div>
            </div>
        </div>
    </div>
    <section class="grid3d vertical portfolio" id="portfolio">
        <div class="container">
            <div class="teams-heading text-center col-md-8 offset-md-2 col-sm-12 text-center">
                <!-- <span>Heros Behind The Company</span> -->
                <h3 class="teams-heading" style="direction: rtl">کارهای اخیر ما</h3>

                <p class="heading_space" style="direction: rtl">
                    کارهای اخیر مبل اتحاد
                </p>
            </div>
        </div>

        <div class="grid-wrap">
            <div class="grid">
                <div class="row" style="margin-right: 0;margin-left: 0">
                    <?php
                    $stmt = $con->prepare('SELECT * FROM `sofas` ORDER BY `id` DESC LIMIT ?');
                    $limit = 9;
                    $stmt->bind_param('i', $limit);
                    $stmt->execute();
                    $sofas = $stmt->get_result();
                    while ($sofa = $sofas->fetch_assoc()) {
                        ?>
                        <a href="single-sofa?q=<?php echo $sofa['id']; ?>" style="color: inherit">
                            <div class="col-sm">
                                <div class="blog-item-box">
                                    <figure class="blog-item">
                                        <div class="image">
                                            <img style="width: 310px;height: 189.88px"
                                                 src="img/Sofas/<?php echo $sofa['id']; ?>/1.jpg"
                                                 alt="<?php echo $sofa['name']; ?>-مبل اتحاد"/>

                                            <i class="icon-armchair-chair-streamline" style="color: #fff;" aria-hidden="true"></i>

                                            <div class="date"><span class="day" style="padding-bottom: 50%">مبل</span><span
                                                        class="month"></span></div>
                                        </div>
                                        <figcaption>
                                            <h3 style="direction: rtl">
                                                <?php echo $sofa['name']; ?>
                                            </h3>
                                            <p style="direction: rtl">
                                                <?php echo substr($sofa['description'],0,60);if (strlen($sofa['description']) > 60){echo '...';} ?>
                                            </p>
                                            <!--<a href="sofa-categories" class="read-more"
                                               style="font-weight: bold;font-size: 15px;">
                                                مشاهده بیشتر
                                            </a>
                                            -->
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                        </a>
                    <?php } ?>
                </div>
            </div>
        </div><!-- /grid-wrap --><div class="content gallery-content">
                <div>

                </div>
            <span class="loading"></span>
            <span class="icon close-content">&times;</span>
        </div>
    </section>

    <!--================== Features section =====================-->
    <div id="features">
        <div class="text-center features-caption features">
            <h3 style="direction: rtl">به تولیدی ما خوش آمدید</h3>
            <h4 style="direction: rtl">ما کی هستیم و چی کار می تونیم بکنیم</h4>
            <p style="direction: rtl">
                لورم ایپسوم متن طولانی ساختگی
            </p>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm bottom-space">
                    <div class="feature-box">
                        <i class="fa fa-image fa-5x fa-icon-image"></i>
                        <h3 style="direction: rtl" class="heading-tertiary u-margin-bottom-small">
                            طرح های خلاقانه
                        </h3>
                        <p class="feature-box__text" style="direction: rtl">
                            استفاده از طرح های خلاقانه برای تولید انواع مبل ها در تولیدی
                        </p>
                    </div>
                </div>
                <div class="col-sm bottom-space">
                    <div class="feature-box">
                        <i class="fa fa-lightbulb-o fa-5x fa-icon-image"></i>
                        <h3 style="direction: rtl" class="heading-tertiary u-margin-bottom-small">قیمت مناسب</h3>
                        <p class="feature-box__text" style="direction: rtl">
                            قیمت خوب به دلیل نبود واسط و مستقیم آوردن وسایل از تولیدی
                        </p>
                    </div>
                </div>
                <div class="col-sm bottom-space">
                    <div class="feature-box">
                        <i class="fa fa-5x fa-code fa-icon-image"></i>
                        <h3 class="heading-tertiary u-margin-bottom-small" style="direction: rtl">کیفیت خوب</h3>
                        <p class="feature-box__text" style="direction: rtl">
                            کیفیت خوب به دلیل سخت گیری در کارها و خلاقیت در طرح ها از خصوصیات محصولات ما
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="features-section-2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm features-section-2-col-1 hover-effect">
                    </div>
                    <div class="col-sm features-section-2-col-2 ">
                        <div class="features-section-2-col-2__content">
                            <h2 style="direction: rtl">تلاش می کنیم بهترین باشیم و کارهای فوق العاده انجام دهیم</h2>
                            <p style="direction: rtl">
                                لورم ایپسوم متن ساختگی طولانی
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--================== Services section =====================-->
    <div id="services" style="padding-top: 32px;">
        <!--

        <div class="container features">
            <div class="row">
                <div class="hi-man">
                    <div class="col-lg-3 col-md-6 col-sm-12 bottom-space">
                        <a href="">
                            <div class="folded-corner service_tab_1">
                                <div class="text" style="text-align: center">
                                    <i class="icon-armchair-chair-streamline fa-5x fa-icon-image"></i>
                                    <h3 class="item-title" style="text-align:center;direction: rtl"> مبل ها</h3>
                                    <p style="direction: rtl">

                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 bottom-space">
                        <a href="">
                            <div class="folded-corner service_tab_1">
                                <div class="text" style="text-align: center">
                                    <i class="fa fa-bed fa-5x fa-icon-image"></i>
                                    <h3 class="item-title" style="direction: rtl"> تخت ها</h3>
                                    <p style="direction: rtl">
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 bottom-space">
                        <div class="folded-corner service_tab_1">
                            <div class="text" style="text-align: center">
                                <i class="fa fa-building fa-5x fa-icon-image"></i>
                                <h3 class="item-title" style="direction: rtl"> بوفه</h3>
                                <p style="direction: rtl">
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 bottom-space">
                    <div class="folded-corner service_tab_1">
                        <div class="text" style="text-align: center">
                            <i class="fa fa-diamond fa-5x fa-icon-image"></i>
                            <h3 class="item-title" style="direction: rtl"> برندینگ</h3>
                            <p style="direction: rtl">
                                لورم ایپسوم متن ساختگی طولانی برای برندینگ
                            </p>
                        </div>
                    </div>
                <div class="col-sm">
                    <div class="blog-item-box">
                        <figure class="blog-item">
                            <div class="image">
                                <img style="width: 310px;height: 189.88px"
                                     src="img/Mothers/sofa.jpg"
                                     alt="مبل-مبل اتحاد"/>

                                <i class="icon-armchair-chair-streamline" style="color: #fff;" aria-hidden="true"></i>

                                <div class="date"><span class="day" style="padding-bottom: 50%">مبل</span><span
                                            class="month"></span></div>
                            </div>
                            <figcaption>
                                <h3 style="direction: rtl">
                                    مبل
                                </h3>
                                <p style="direction: rtl">

                                </p>
                                <a href="sofa-categories" class="read-more"
                                   style="font-weight: bold;font-size: 15px;">
                                    مشاهده بیشتر
                                </a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class="col-sm">
                    <div class="blog-item-box">
                        <figure class="blog-item">
                            <div class="image">
                                <img style="width: 310px;height: 189.88px"
                                     src="img/Mothers/bed.jpg"
                                     alt="مبل-مبل اتحاد"/>

                                <i class="fa fa-bed" aria-hidden="true"></i>

                                <div class="date"><span class="day" style="padding-bottom: 50%">تخت</span><span
                                            class="month"></span></div>
                            </div>
                            <figcaption>
                                <h3 style="direction: rtl">
                                    تخت
                                </h3>
                                <p style="direction: rtl">

                                </p>
                                <a href="sofa-categories" class="read-more"
                                   style="font-weight: bold;font-size: 15px;">
                                    مشاهده بیشتر
                                </a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class="col-sm">
                    <div class="blog-item-box">
                        <figure class="blog-item">
                            <div class="image">
                                <img style="width: 310px;height: 189.88px"
                                     src="img/Mothers/cabinet.jpg"
                                     alt="مبل-مبل اتحاد"/>

                                <i class="fa fa-building" style="color: #fff;" aria-hidden="true"></i>

                                <div class="date"><span class="day" style="padding-bottom: 50%">بوفه</span><span
                                            class="month"></span></div>
                            </div>
                            <figcaption>
                                <h3 style="direction: rtl">
                                    بوفه
                                </h3>
                                <p style="direction: rtl">

                                </p>
                                <a href="sofa-categories" class="read-more"
                                   style="font-weight: bold;font-size: 15px;">
                                f    مشاهده بیشتر
                                </a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                </div>
            </div>
        </div>
        -->
    </div>


    <!--================== Portfolio section =====================-->

    <!--================== Team section =====================-->

     <div id="teams" style="background-color: inherit">
    <!--
        <div class="container">
            <div class="teams-heading text-center col-md-8 offset-md-2 col-sm-12 text-center">
                <span>Heros Behind The Company</span>
                <h1 class="teams-heading" style="direction: rtl">تیم خلاق</h1>

                <p class="heading_space" style="direction: rtl">
                    تیم ما متشکل از افراد زبردست و خلاق در تخصص های خودشان است
                </p>
            </div>

            <div class="row">
                <?php
    /*$stmt = $con->prepare('SELECT *,`users`.`id` AS `user_id`, `board_words`.`id` AS `board_id` FROM `board_words` INNER JOIN `users` ON `users`.`id` = `board_words`.`user` WHERE `confirmed` != 0 GROUP BY `board_words`.`id` ORDER BY `board_words`.`id` DESC LIMIT 3');
    $stmt->execute();
    $words = $stmt->get_result();
    while ($word = $words->fetch_assoc()) {
    */
        ?>
                    <div class="col-sm">
                        <div class="teams-item-box">
                            <div class="teams-item" style="height: 316px;">
                                <div class="profile-image"><img src="img/Users/<?php //echo $word['user_id']; ?>.jpg"
                                                                alt="<?php //echo $word['name']; ?>"/></div>
                                <div>
                                    <h3 style="direction: rtl;font-family: Vazir"><?php //echo $word['name']; ?></h3>
                                    <h5 style="direction: rtl"><?php //echo $word['role']; ?></h5>
                                    <p style="direction: rtl">
                                        <?php //echo $word['text']; ?>
                                    </p>
                                    <div class="icons">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php //} ?>
            </div>

        </div>
    </div>
     -->

    <!--================== Feedback section =====================-->
         <!--
    <div id="testimonials" class="testimonials">
        <div class="cd-testimonials-wrapper cd-container">
            <ul class="cd-testimonials">
                <?php
    /*$stmt = $con->prepare('SELECT * FROM `contact_us` WHERE `confirm` != 0 ORDER BY `id` DESC LIMIT ?');
    $limit = 12;
    $stmt->bind_param('i', $limit);
    $stmt->execute();
    $contacts = $stmt->get_result();
    $all = $contacts;
    for ($i = 0; $i < 4; $i++) {
        $contact = $contacts->fetch_assoc();
    */
    ?>
                    <li>
                        <h2>
                            نظرات کاربران
                        </h2>
                        <p style="direction: rtl">
                            <?php //echo $contact['text']; ?>
                        </p>
                        <div class="cd-author">
                            <ul class="cd-author-info">
                                <li style="direction: rtl"><?php //echo $contact['name'] ?></li>
                            </ul>
                        </div>
                    </li>
                <?php //} ?>
            </ul>

            <a href="#0" class="cd-see-all">دیدن همه</a>
        </div>

        <div class="cd-testimonials-all">
            <div class="cd-testimonials-all-wrapper">
                <ul>
                    <?php //while ($contact = $all->fetch_assoc()) { ?>
                        <li class="cd-testimonials-item">
                            <p style="direction: rtl">
                                <?php //echo $contact['text']; ?>
                            </p>

                            <div class="cd-author">
                                <ul class="cd-author-info">
                                    <li style="direction: rtl"><?php //echo $contact['name']; ?></li>
                                </ul>
                            </div>
                        </li>
                    <?php //} ?>
                </ul>
            </div>

            <a href="#0" class="close-btn">Close</a>
        </div>
        -->
    </div>


    <!--================== Blog section =====================-->
<!--    <div id="blog" class="blog">-->
<!--        <div class="container">-->
<!--            <div class="teams-heading text-center col-md-8 offset-md-2 col-sm-12 text-center">-->
<!--                 <span>Heros Behind The Company</span> --><!--                <h1 class="teams-heading" style="position: relative;color: #fff;direction: rtl">-->
<!--                    انواع مبل ها-->
<!--                </h1>-->
<!--            </div>-->
<!--            <a href="sofa-categories">-->
<!--                <h6 style="text-align: left;font-weight: bold;margin-top: 28px;">-->
<!--                    <i class="fa fa-arrow-circle-left"></i>-->
<!--                    دسته های بیشتر-->
<!---->
<!--                </h6>-->
<!--            </a>-->
<!--            <div class="row">-->
<!--                --><?php
//                $stmt = $con->prepare('SELECT * FROM `sofa_categories` ORDER BY `id` DESC LIMIT ?');
//                $limit = 3;
//                $stmt->bind_param('i', $limit);
//                $stmt->execute();
//                $categories = $stmt->get_result();
//                while ($category = $categories->fetch_assoc()) {
//                    ?>
<!--                    <div class="col-sm">-->
<!--                        <div class="blog-item-box">-->
<!--                            <figure class="blog-item">-->
<!--                                <div class="image">-->
<!--                                    <img style="width: 310px;height: 189.88px"-->
<!--                                         src="img/Categories/--><?php //echo $category['id']; ?><!--.jpg"-->
<!--                                         alt="sofa---><?php //echo $category['caption']; ?><!--"/>-->
<!---->
<!--                                    <i class="fa fa-list-alt" aria-hidden="true"></i>-->
<!---->
<!--                                    <div class="date"><span class="day" style="padding-bottom: 50%">مبل</span><span-->
<!--                                                class="month"></span></div>-->
<!--                                </div>-->
<!--                                <figcaption>-->
<!--                                    <h3 style="direction: rtl">-->
<!--                                        --><?php //echo $category['caption']; ?>
<!--                                    </h3>-->
<!--                                    <p style="direction: rtl">-->
<!--                                        --><?php //echo $category['description']; ?>
<!--                                    </p>-->
<!--                                    <a href="single-category?q=--><?php //echo $category['id']; ?><!--" class="read-more"-->
<!--                                       style="font-weight: bold;font-size: 15px;">-->
<!--                                        مشاهده بیشتر-->
<!--                                    </a>-->
<!--                                </figcaption>-->
<!--                            </figure>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                --><?php //} ?>
<!--                -->
<!--                <div class="col-sm">-->
<!--                  <div class="blog-item-box">-->
<!--                    <figure class="blog-item">-->
<!--                        <div class="image">-->
<!--                          <img src="./img/sample84.jpg" alt="sample87"/>-->
<!--                          <i class="fa fa-clock-o" aria-hidden="true"></i>-->
<!--                          <div class="date"><span class="day">22</span>-->
<!--                            <span class="month">خرداد</span></div>-->
<!--                        </div>-->
<!--                        <figcaption>-->
<!--                          <h3>-->
<!--                              نمونه کار سلطنتی-->
<!--                          </h3>-->
<!--                          <p>-->
<!--                              مبل های سلطنتی با لعاب طلا در تاج با کیفیت بالا و قیمت پایین فقط در مبل اتحاد-->
<!--                          </p>-->
<!--                          <a href="#" class="read-more">Read More</a>-->
<!--                        </figcaption>-->
<!--                      </figure>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-sm">-->
<!--                  <div class="blog-item-box">-->
<!--                    <figure class="blog-item">-->
<!--                        <div class="image">-->
<!--                          <img src="./img/sample87.jpg" alt="sample84"/>-->
<!--                          <i class="fa fa-clock-o" aria-hidden="true"></i>-->
<!--                          <div class="date"><span class="day">26</span><span class="month">شهریور</span></div>-->
<!--                        </div>-->
<!--                        <figcaption>-->
<!--                          <h3>یک نمونه کار دیگه از مبل های سلطنتی</h3>-->
<!--                          <p>-->
<!--                              می دونید ما به چی نیاز داریم، سرگرمی؟ ما به نگرش نیاز داریم. شما نمی تونید باحال باشید اگه نگرش نداشته باشید؛ پس انتقادات و پیشنهادات خود را از انتهای همین صفحه بفرستید تا نگرش ما را به سبک خود دراورید-->
<!--                          </p>-->
<!--                          <a href="#" class="read-more">Read More</a>-->
<!--                        </figcaption>-->
<!--                      </figure>-->
<!--                  </div>-->
<!--                </div>-->
<!--                  -->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->


    <div id="contact" class="contact">
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3237.939578238468!2d51.07232611460736!3d35.75228983368028!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8dec74ca6b660f%3A0x2e1f5df3bb9ad5f3!2sPars%20Aqua%20Village!5e0!3m2!1sen!2scz!4v1566673378397!5m2!1sen!2scz"
                    width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-5 offset-md-2 col-lg-6 offset-lg-0">
                    <h1 class="contact-heading" style="direction: rtl">بیاید با هم در ارتباط باشیم!</h1>
                    <p style="direction: rtl">
                        بیاید با مبل اتحاد در ارتباط باشید تا با هم پیشرفت کنیم
                    </p>

                    <div class="row margin-15px-bottom">
                        <div class="col-sm-1 no-padding">
                            <div class="contact-icon text-blue">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col-sm-11">
                            <p class="text-small" style="text-align:center;direction: rtl">
                                شهرک ولی عصر، خیابان وحدت، میدان رجایی، خیابان سجاد شمالی، کوچه طالقانی، پلاک 43، مبل اتحاد
                            </p>
                        </div>
                    </div>

                    <div class="row margin-15px-bottom">
                        <div class="col-sm-1 no-padding">
                            <div class="contact-icon text-blue">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col-sm-11">
                            <p class="text-small">
                                021-66141123
                            </p>
                        </div>
                    </div>

                    <div class="row margin-15px-bottom">
                        <div class="col-sm-1 no-padding">
                            <div class="contact-icon text-blue">
                                <i class="fa fa-globe" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col-sm-11 xs-margin-50px-bottom">
                            <p class="text-small">
                                etehad-sofas.ir
                                <br>
                                etehad-sofas.com
                            </p>
                        </div>
                    </div>


                </div>


                <div class="col-sm-6 col-md-5 offset-md-2 col-lg-6 offset-lg-0">
                    <!-- Starting of ajax contact form -->
                    <form class="contact__form" method="post" action="send">
                        <!-- Element of the ajax contact form -->
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input name="name" type="text" class="form-control" style="direction: rtl"
                                       placeholder="نام" required>
                            </div>
                            <div class="col-md-6 form-group">
                                <input name="email" type="email" class="form-control" placeholder="ایمیل"
                                       style="direction: rtl" required>
                            </div>
                            <div class="col-md-6 form-group">
                                <input name="phone" type="text" class="form-control" placeholder="شماره تلفن"
                                       style="direction: rtl" required>
                            </div>
                            <div class="col-md-6 form-group">
                                <input name="subject" type="text" class="form-control" placeholder="موضوع"
                                       style="direction: rtl" required>
                            </div>
                            <div class="col-12 form-group">
                                <textarea name="massage" class="form-control" rows="3" placeholder="پیغام"
                                          style="direction: rtl" required></textarea>
                            </div>
                            <div class="col-12" style="text-align: right">
                                <input name="submit" type="submit" class="button-style" value="ارسال"
                                       style="direction: rtl">
                            </div>
                        </div>


                    </form>
                    <!-- Ending of ajax contact form -->
                    <!-- Starting of successful form message -->
                    <div class="row">
                        <div class="col-12">
                            <div class="alert alert-success contact__msg" style="display: none" role="alert">
                                پیام شما با موفقیت ارسال شد
                            </div>
                        </div>
                    </div>
                    <!-- Ending of successful form message -->
                </div>
            </div>
        </div>
    </div>
<?php
require '__include/__footer.php';