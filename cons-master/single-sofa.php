<?php
if (isset($_GET['q'])) {
    $c = 1;
    require '__include/__config.php';
    $con = new mysqli(Host,User,Password,DB);
    $con->set_charset('utf8');
    $stmt = $con->prepare('SELECT *, `sofas`.id AS `sofa_id`, `sofas`.description AS `sofa_description`, `sofa_categories`.id FROM `sofas` INNER JOIN `sofa_categories` ON `sofa_categories`.id = `sofas`.category WHERE `sofas`.`id` = ? ORDER BY sofas.`id` DESC');
    $stmt->bind_param('i', $_GET['q']);
    $stmt->execute();
    $sofas = $stmt->get_result();
    $sofa = $sofas->fetch_assoc();
    $Page_Title = $sofa['name'];
    require '__include/__header.php';
    ?>
    <div id="showcase">
        <div class="container showcase">
            <div class="full-width text-center showcase-caption mt-30">
                <h4>مبل اتحاد</h4>
                <h1 style="direction: rtl">
                    <?php echo $sofa['name']; ?>
                </h1>
                <p>کیفیت بالا با قیمت مناسب</p>
            </div>
        </div>
    </div>
    <div style="margin-top: 20px;background-color: #eee;padding: 20px 0 20px 0;">
        <div class="text-center col-md-8 offset-md-2 col-sm-12 text-center">
            <h3 style="direction: rtl"><?php echo $sofa['name']; ?></h3>
            <img class="" style="width:100%" src="img/Sofas/<?php echo $sofa['sofa_id']; ?>/1.jpg"
                 alt="sofa-<?php echo $sofa['name']; ?>"/>
            <p style="direction: rtl">
            <br>
            <h4 style="direction: rtl">
                قیمت:

                <?php echo number_format($sofa['price']); ?>
                تومان
            </h4>
            <br>
            <p style="direction: rtl">
                توضیحات:
                <?php echo $sofa['sofa_description']; ?>
            </p>
            <?php if ($sofa['available'] == 0) {
                echo '<h3 style="color:red;">موجود نیست</h3>';
            } ?>
            <?php if ($sofa['images_count'] > 1) {
                echo '<div style="padding:0 0 30px 0;background-color: #fff"><br><br><br><h1>عکس های بیشتر</h1><br><br>';
                for ($i = 2; $i <= $sofa['images_count']; $i++) {
                    ?>
                    <img class="" style="width: 100%;max-height: 95vh;"
                         src="<?php echo 'img/Sofas/'.$sofa['sofa_id'].'/'.$i.'.jpg'; ?>"
                         alt="sofa-<?php echo $sofa['name']; ?>">
                    <?php
                }
                echo '</div>';
                ?>
                <?php
            } ?>
        </div>
    </div>

    <?php
    echo '';
    require '__include/__footer.php';
}
else{
    header('location: /');
}