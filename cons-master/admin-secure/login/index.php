<?php
require '../../__include/__config.php';
session_start();
$con = new mysqli(Host,User,Password,DB);
$con->set_charset('utf8');
if (isset($_POST['name'],$_POST['password']) && @$_POST['name'] != '' && @$_POST['password'] != ''){
    $stmt = $con->prepare('SELECT * FROM `users` WHERE `name` = ?');
    $stmt->bind_param('s',$_POST['name']);
    $stmt->execute();
    $users = $stmt->get_result();
    if ($users->num_rows == 1){
        $user = $users->fetch_assoc();
        if (password_verify($_POST['password'],$user['password'])){
            $_SESSION['name'] = $user['name'];
            $_SESSION['role'] = $user['role'];
            $_SESSION['id'] = $user['id'];
            $_SESSION['accessibility'] = $user['accessibility'];
            header('location: /admin-panel/options/examples');
            // TODO give a verified location
        }
    }
}
?>
<style>
    form{
        margin: auto;
        width: 50%;
        background-color: #eee;
        text-align: center;
        padding: 20px;
        position: relative;
        top: 30%;
        box-shadow: 1px 1px 3px rgba(0,0,0,.3);
    }
    label{
        display: block;
        margin: 20px;
    }
</style>
<form method="post">
    <?php if (isset($_GET['error'])){echo '<h1 style="color:red;">خطا</h1>';} ?>
    <label>
        نام کاربری
        <input type="text" name="name">
    </label>
    <label>
        رمز عبور
        <input type="password" name="password">
    </label>
    <input type="submit" value="ورود">
</form>
