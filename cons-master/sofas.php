<?php
$Page_Title = 'مبل ها';
require '__include/__header.php';
?>
<div id="showcase">
    <div class="container showcase">
        <div class="full-width text-center showcase-caption mt-30">
            <h4>مبل اتحاد</h4>
            <h1>انواع مبل های موجود در مبل اتحاد</h1>
            <p>کیفیت بالا با قیمت مناسب</p>
        </div>
    </div>
</div>
<section class="grid3d vertical portfolio" id="portfolio">
    <div class="container">
        <div class="teams-heading text-center col-md-8 offset-md-2 col-sm-12 text-center">
            <h1>مبل ها</h1>
        </div>
    </div>

    <div class="grid-wrap">
        <div class="grid">
            <div class="row" style="margin-right: 0;margin-left: 0">
                <?php
                $stmt = $con->prepare('SELECT * FROM `sofas` ORDER BY `id` DESC');
                $stmt->execute();
                $sofas = $stmt->get_result();
                while ($sofa = $sofas->fetch_assoc()) {
                    ?>
                    <a href="single-sofa?q=<?php echo $sofa['id']; ?>" style="color: inherit">
                        <div class="col-sm">
                            <div class="blog-item-box">
                                <figure class="blog-item">
                                    <div class="image">
                                        <img style="width: 310px;height: 189.88px"
                                             src="img/Sofas/<?php echo $sofa['id']; ?>/1.jpg"
                                             alt="<?php echo $sofa['name']; ?>-مبل اتحاد"/>

                                        <i class="icon-armchair-chair-streamline" style="color: #fff;" aria-hidden="true"></i>

                                        <div class="date"><span class="day" style="padding-bottom: 50%">مبل</span><span
                                                    class="month"></span></div>
                                    </div>
                                    <figcaption>
                                        <h3 style="direction: rtl">
                                            <?php echo $sofa['name']; ?>
                                        </h3>
                                        <p style="direction: rtl">
                                            <?php echo substr($sofa['description'],0,60);if (strlen($sofa['description']) > 60){echo '...';} ?>
                                        </p>
                                        <!--<a href="sofa-categories" class="read-more"
                                           style="font-weight: bold;font-size: 15px;">
                                            مشاهده بیشتر
                                        </a>
                                        -->
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div><!-- /grid-wrap -->
    <div class="content gallery-content">
        <div>
        <span class="loading"></span>
        <span class="icon close-content">&times;</span>
    </div>
</section>
<?php
$no_js = 1;
require '__include/__footer.php';