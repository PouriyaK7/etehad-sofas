<?php
require '__include/__header.php';
if (isset(
    $_POST['name'],
    $_POST['email'],
    $_POST['phone'],
    $_POST['subject'],
    $_POST['massage']
)){
    $stmt = $con->prepare('INSERT INTO `contact_us`(`name`,`email`,`phone`,`subject`,`text`) VALUES (?,?,?,?,?)');
    $stmt->bind_param('sssss',$_POST['name'],$_POST['email'],$_POST['phone'],$_POST['subject'],$_POST['text']);
    $stmt->execute();
    header('location: /');
}