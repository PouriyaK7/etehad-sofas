<?php
if (isset($_GET['q'])) {
    require '__include/__config.php';
    $con = new mysqli(Host,User,Password,DB);
    $con->set_charset('utf8');
    $stmt = $con->prepare('SELECT * FROM sofa_categories where sofa_categories.id = ?');
    $stmt->bind_param('i',$_GET['q']);
    $stmt->execute();
    $categories = $stmt->get_result();
    $category = $categories->fetch_assoc();
    $c = 1;
    $Page_Title = $category['caption'];
    require '__include/__header.php';
    ?>
    <div id="showcase">
        <div class="container showcase">
            <div class="full-width text-center showcase-caption mt-30">
                <h4 style="direction: rtl">مبل اتحاد</h4>
                <h1 style="direction: rtl">تمامی مبل های دسته بندی <?php echo $category['caption']; ?></h1>
                <p style="direction: rtl">کیفیت بالا با قیمت مناسب</p>
            </div>
        </div>
    </div>
    <?php
    $stmt = $con->prepare('SELECT *, sofas.id AS `sofa_id`, sofas.description AS sofa_description FROM sofas 
    INNER JOIN sofa_categories ON sofa_categories.id = sofas.category
    WHERE
        sofa_categories.id = ?
    GROUP BY sofas.id DESC ');
    $stmt->bind_param('i',$_GET['q']);
    $stmt->execute();
    $sofas = $stmt->get_result();
    if ($sofas->num_rows == 0){
        echo '<h1 style="text-align: center;margin: 100px;">هیچ مبلی با ابن دسته وجود ندارد</h1>';
    }
    else {
        ?>
        <section class="grid3d vertical portfolio" id="portfolio">
            <div class="container">
                <div class="teams-heading text-center col-md-8 offset-md-2 col-sm-12 text-center">
                    <!-- <span>Heros Behind The Company</span> -->
                </div>
            </div>

            <div class="grid-wrap">
                <div class="grid">
                    <div class="row" style="margin-right: 0;margin-left: 0">
                        <?php
                        while ($sofa = $sofas->fetch_assoc()) {
                            ?>
                            <a href="single-sofa?q=<?php echo $sofa['id']; ?>" style="color: inherit">
                                <div class="col-sm">
                                    <div class="blog-item-box">
                                        <figure class="blog-item">
                                            <div class="image">
                                                <img style="width: 310px;height: 189.88px"
                                                     src="img/Sofas/<?php echo $sofa['id']; ?>/1.jpg"
                                                     alt="<?php echo $sofa['name']; ?>-مبل اتحاد"/>

                                                <i class="icon-armchair-chair-streamline" style="color: #fff;" aria-hidden="true"></i>

                                                <div class="date"><span class="day" style="padding-bottom: 50%">مبل</span><span
                                                            class="month"></span></div>
                                            </div>
                                            <figcaption>
                                                <h3 style="direction: rtl">
                                                    <?php echo $sofa['name']; ?>
                                                </h3>
                                                <p style="direction: rtl">
                                                    <?php echo substr($sofa['description'],0,60);if (strlen($sofa['description']) > 60){echo '...';} ?>
                                                </p>
                                                <!--<a href="sofa-categories" class="read-more"
                                                   style="font-weight: bold;font-size: 15px;">
                                                    مشاهده بیشتر
                                                </a>
                                                -->
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div><!-- /grid-wrap -->
            <div class="content gallery-content">
                    <span class="loading"></span>
                    <span class="icon close-content">&times;</span>
            </div>
        </section>
        <?php
    }
    require '__include/__footer.php';
}
else{
    header('location: /');
}